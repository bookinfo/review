# Bookinfo Review Service

Review service has been developed on Java

## License

Fair Use

## How to run with Docker

```bash
docker-compose up
```
# Test

* Health check path `/health`
* Test with path `/review/1`